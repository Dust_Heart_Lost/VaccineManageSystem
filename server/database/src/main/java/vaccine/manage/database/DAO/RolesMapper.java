package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Roles;

public interface RolesMapper {
    List<Roles> selectAll();

    int insert(Roles record);

    int insertSelective(Roles record);

    Roles selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Roles record);

    int updateByPrimaryKey(Roles record);

    List<Roles> fuzzySearch(String searchModel);
}