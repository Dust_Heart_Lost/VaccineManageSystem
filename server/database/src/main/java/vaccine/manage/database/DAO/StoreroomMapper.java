package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Storeroom;

public interface StoreroomMapper {
    List<Storeroom> selectAll();

    int insert(Storeroom record);

    int insertSelective(Storeroom record);

    Storeroom selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Storeroom record);

    int updateByPrimaryKey(Storeroom record);

    List<Storeroom> fuzzySearch(String searchModel);
}