package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Inflow;

public interface InflowMapper {
    List<Inflow> selectAll(Integer id);

    int deleteByPrimaryKey(Integer id);

    int insert(Inflow record);

    int insertSelective(Inflow record);

    Inflow selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Inflow record);

    int updateByPrimaryKey(Inflow record);

    List<Inflow> fuzzySearch(String searchModel);

    int updateAddMoney(Inflow record);

    int updateSubMoney(Inflow record);
}