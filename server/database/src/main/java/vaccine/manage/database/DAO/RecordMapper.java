package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Record;

public interface RecordMapper {
    List<Record> selectAll();

    int deleteByPrimaryKey(Integer id);

    int insert(Record record);

    int insertSelective(Record record);

    Record selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Record record);

    int updateByPrimaryKey(Record record);

    List<Record> fuzzySearch(String searchModel);
}