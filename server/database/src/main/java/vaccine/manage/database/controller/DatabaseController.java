package vaccine.manage.database.controller;

import java.io.InputStream;
import java.util.List;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import vaccine.manage.common.MyData;
import vaccine.manage.database.DAO.CategoryMapper;
import vaccine.manage.database.DAO.InflowMapper;
import vaccine.manage.database.DAO.MyusersMapper;
import vaccine.manage.database.DAO.RecordMapper;
import vaccine.manage.database.DAO.RolesMapper;
import vaccine.manage.database.DAO.StoreroomMapper;
import vaccine.manage.database.DAO.SuppliersMapper;
import vaccine.manage.database.DAO.VaccineMapper;
import vaccine.manage.database.Model.Category;
import vaccine.manage.database.Model.Inflow;
import vaccine.manage.database.Model.Myusers;
import vaccine.manage.database.Model.Roles;
import vaccine.manage.database.Model.Record;
import vaccine.manage.database.Model.Storeroom;
import vaccine.manage.database.Model.Suppliers;
import vaccine.manage.database.Model.Vaccine;

@RestController
public class DatabaseController {
    static SqlSession session = null;

    /**
     * c
     * 错误码
     * 1000：成功执行
     * 3001：SqlMapperConfig.xml找不到
     * 3002：请求操作的表找不到
     * 3003：insert操作数据不符合规范
     * 3004：select操作没有数据
     * 3005：update操作数据不符合数据库约束
     * 3006：update操作没有对应id的数据
     * 3007: 更新疫苗表金额字段错误
     */
    private <T> MyData convert(List<T> t, String DAO) {
        session.close();
        if (t.size() == 0) {
            return new MyData(3004, null, "find no data");
        }
        return new MyData(1000, new PageInfo<T>(t), "select all from " + DAO + " success");

    }

    private <T> MyData convert(T t, String DAO) {
        session.close();
        if (t == null) {
            return new MyData(3004, null, "find no data");
        }
        return new MyData(1000, t, "select all from " + DAO + " success");

    }

    private MyData insertConvert(int t, String DAO) {
        if (t == 1) {
            session.commit();
            session.close();
            return new MyData(1000, 1, "insert into " + DAO + " success");
        } else {
            session.close();
            return new MyData(3003, 1, "insert into " + DAO + " fail");
        }

    }

    private MyData updateConvert(int t, String DAO) {
        if (t == 1) {
            session.commit();
            session.close();
            return new MyData(1000, 1, "update " + DAO + " success");
        } else {
            session.close();
            return new MyData(3006, 1, "update " + DAO + " fail");
        }
    }

    @PostMapping(value = "queryAll/{DAO}")
    public MyData queryAll(@PathVariable String DAO, @RequestBody MyData parameters) {
        try (InputStream inputStream = Resources.getResourceAsStream("SqlMapperConfig.xml")) {
            session = new SqlSessionFactoryBuilder().build(inputStream).openSession();
        } catch (IOException e) {
            MyData myData = new MyData(3001, null, "SqlMapperConfig.xml can not find");
            return myData;
        }
        JSONObject parameter = (JSONObject) JSONObject.toJSON(parameters.getData());
        PageHelper.startPage((int) parameter.get("startPage"), (int) parameter.get("pageSize"));
        switch (DAO) {
            case "Category":
                return convert(session.getMapper(CategoryMapper.class).selectAll(), DAO);
            case "Inflow":
                return convert(session.getMapper(InflowMapper.class).selectAll((int) parameter.get("vaccineID")), DAO);
            case "Myusers":
                return convert(session.getMapper(MyusersMapper.class).selectAll(), DAO);
            case "Record":
                return convert(session.getMapper(RecordMapper.class).selectAll(), DAO);
            case "Roles":
                return convert(session.getMapper(RolesMapper.class).selectAll(), DAO);
            case "Storeroom":
                return convert(session.getMapper(StoreroomMapper.class).selectAll(), DAO);
            case "Suppliers":
                return convert(session.getMapper(SuppliersMapper.class).selectAll(), DAO);
            case "Vaccine":
                return convert(session.getMapper(VaccineMapper.class).selectAll(), DAO);
            default:
                return new MyData(3002, null, "can not find table");
        }
    }

    @PostMapping(value = "queryById/{DAO}")
    public MyData queryById(@PathVariable String DAO, @RequestBody MyData parameters) {
        try (InputStream inputStream = Resources.getResourceAsStream("SqlMapperConfig.xml")) {
            session = new SqlSessionFactoryBuilder().build(inputStream).openSession();
        } catch (IOException e) {
            MyData myData = new MyData(3001, null, "SqlMapperConfig.xml can not find");
            return myData;
        }
        JSONObject parameter = (JSONObject) JSONObject.toJSON(parameters.getData());
        switch (DAO) {
            case "Category":
                return convert(session.getMapper(CategoryMapper.class).selectByPrimaryKey((int) parameter.get("id")),
                        DAO);
            case "Inflow":
                return convert(session.getMapper(InflowMapper.class).selectByPrimaryKey((int) parameter.get("id")),
                        DAO);
            case "Myusers":
                return convert(
                        session.getMapper(MyusersMapper.class).selectByUsername(parameter.get("username").toString()),
                        DAO);
            case "Record":
                return convert(session.getMapper(RecordMapper.class).selectByPrimaryKey((int) parameter.get("id")),
                        DAO);
            case "Roles":
                return convert(session.getMapper(RolesMapper.class).selectByPrimaryKey((int) parameter.get("id")), DAO);
            case "Storeroom":
                return convert(session.getMapper(StoreroomMapper.class).selectByPrimaryKey((int) parameter.get("id")),
                        DAO);
            case "Suppliers":
                return convert(session.getMapper(SuppliersMapper.class).selectByPrimaryKey((int) parameter.get("id")),
                        DAO);
            case "Vaccine":
                return convert(session.getMapper(VaccineMapper.class).selectByPrimaryKey((int) parameter.get("id")),
                        DAO);
            default:
                return new MyData(3002, null, "can not find table");
        }
    }

    @PostMapping(value = "insert/{DAO}")
    public MyData insert(@PathVariable String DAO, @RequestBody MyData parameters) {
        try (InputStream inputStream = Resources.getResourceAsStream("SqlMapperConfig.xml")) {
            session = new SqlSessionFactoryBuilder().build(inputStream).openSession();
        } catch (IOException e) {
            MyData myData = new MyData(3001, null, "SqlMapperConfig.xml can not find");
            return myData;
        }
        JSONObject json = (JSONObject) JSONObject.toJSON(parameters.getData());
        try {
            switch (DAO) {
                case "Category":
                    Category category = JSONObject.toJavaObject(json, Category.class);
                    return insertConvert(session.getMapper(CategoryMapper.class).insert(category), DAO);
                case "Inflow":
                    Inflow inflow = JSONObject.toJavaObject(json, Inflow.class);
                    return insertConvert(session.getMapper(InflowMapper.class).insert(inflow), DAO);
                case "Myusers":
                    Myusers myusers = JSONObject.toJavaObject(json, Myusers.class);
                    return insertConvert(session.getMapper(MyusersMapper.class).insert(myusers), DAO);
                case "Record":
                    Record record = JSONObject.toJavaObject(json, Record.class);
                    return insertConvert(session.getMapper(RecordMapper.class).insert(record), DAO);
                case "Roles":
                    Roles roles = JSONObject.toJavaObject(json, Roles.class);
                    return insertConvert(session.getMapper(RolesMapper.class).insert(roles), DAO);
                case "Storeroom":
                    Storeroom storeroom = JSONObject.toJavaObject(json, Storeroom.class);
                    return insertConvert(session.getMapper(StoreroomMapper.class).insert(storeroom), DAO);
                case "Suppliers":
                    Suppliers suppliers = JSONObject.toJavaObject(json, Suppliers.class);
                    return insertConvert(session.getMapper(SuppliersMapper.class).insert(suppliers), DAO);
                case "Vaccine":
                    Vaccine vaccine = JSONObject.toJavaObject(json, Vaccine.class);
                    return insertConvert(session.getMapper(VaccineMapper.class).insert(vaccine), DAO);
                default:
                    return new MyData(3002, null, "can not find table");
            }

        } catch (Exception e) {
            return new MyData(3003, null, e.getMessage());
        }

    }

    @PostMapping(value = "update/{DAO}")
    public MyData update(@PathVariable String DAO, @RequestBody MyData parameters) {
        try (InputStream inputStream = Resources.getResourceAsStream("SqlMapperConfig.xml")) {
            session = new SqlSessionFactoryBuilder().build(inputStream).openSession();
        } catch (IOException e) {
            MyData myData = new MyData(3001, null, "SqlMapperConfig.xml can not find");
            return myData;
        }
        JSONObject json = (JSONObject) JSONObject.toJSON(parameters.getData());
        try {
            switch (DAO) {
                case "Category":
                    Category category = JSONObject.toJavaObject(json, Category.class);
                    return updateConvert(session.getMapper(CategoryMapper.class).updateByPrimaryKey(category), DAO);
                case "Inflow":
                    Inflow inflow = JSONObject.toJavaObject(json, Inflow.class);
                    return updateConvert(session.getMapper(InflowMapper.class).updateByPrimaryKey(inflow), DAO);
                case "Myusers":
                    Myusers myusers = JSONObject.toJavaObject(json, Myusers.class);
                    return updateConvert(session.getMapper(MyusersMapper.class).updateByPrimaryKey(myusers), DAO);
                case "Record":
                    Record record = JSONObject.toJavaObject(json, Record.class);
                    return updateConvert(session.getMapper(RecordMapper.class).updateByPrimaryKey(record), DAO);
                case "Roles":
                    Roles roles = JSONObject.toJavaObject(json, Roles.class);
                    return updateConvert(session.getMapper(RolesMapper.class).updateByPrimaryKey(roles), DAO);
                case "Storeroom":
                    Storeroom storeroom = JSONObject.toJavaObject(json, Storeroom.class);
                    return updateConvert(session.getMapper(StoreroomMapper.class).updateByPrimaryKey(storeroom), DAO);
                case "Suppliers":
                    Suppliers suppliers = JSONObject.toJavaObject(json, Suppliers.class);
                    return updateConvert(session.getMapper(SuppliersMapper.class).updateByPrimaryKey(suppliers), DAO);
                case "Vaccine":
                    Vaccine vaccine = JSONObject.toJavaObject(json, Vaccine.class);
                    return updateConvert(session.getMapper(VaccineMapper.class).updateByPrimaryKey(vaccine), DAO);
                default:
                    return new MyData(3002, null, "can not find table");
            }

        } catch (Exception e) {
            return new MyData(3005, null, e.getMessage());
        }

    }

    @PostMapping(value = "search/{DAO}")
    public MyData search(@PathVariable String DAO, @RequestBody MyData parameters) {
        try (InputStream inputStream = Resources.getResourceAsStream("SqlMapperConfig.xml")) {
            session = new SqlSessionFactoryBuilder().build(inputStream).openSession();
        } catch (IOException e) {
            MyData myData = new MyData(3001, null, "SqlMapperConfig.xml can not find");
            return myData;
        }
        JSONObject parameter = (JSONObject) JSONObject.toJSON(parameters.getData());
        PageHelper.startPage((int) parameter.get("startPage"), (int) parameter.get("pageSize"));
        switch (DAO) {
            case "Category":
                return convert(
                        session.getMapper(CategoryMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            case "Inflow":
                return convert(
                        session.getMapper(InflowMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            case "Myusers":
                return convert(
                        session.getMapper(MyusersMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            case "Roles":
                return convert(
                        session.getMapper(RolesMapper.class).fuzzySearch(parameter.get("searchModel").toString()), DAO);
            case "Storeroom":
                return convert(
                        session.getMapper(StoreroomMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            case "Suppliers":
                return convert(
                        session.getMapper(SuppliersMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            case "Vaccine":
                return convert(
                        session.getMapper(VaccineMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            case "Record":
                return convert(
                        session.getMapper(RecordMapper.class).fuzzySearch(parameter.get("searchModel").toString()),
                        DAO);
            default:
                return new MyData(3002, null, "can not find table");
        }

    }

    @PostMapping(value = "updateVaccineMoney/{DAO}")
    public MyData updateVaccineMoney(@PathVariable String DAO, @RequestBody MyData parameters) {
        try (InputStream inputStream = Resources.getResourceAsStream("SqlMapperConfig.xml")) {
            session = new SqlSessionFactoryBuilder().build(inputStream).openSession();
        } catch (IOException e) {
            MyData myData = new MyData(3001, null, "SqlMapperConfig.xml can not find");
            return myData;
        }
        JSONObject json = (JSONObject) JSONObject.toJSON(parameters.getData());
        Inflow inflow = JSONObject.toJavaObject(json, Inflow.class);
        try {
            switch (DAO) {
                case "add":
                    return updateConvert(session.getMapper(InflowMapper.class).updateAddMoney(inflow), DAO);
                case "sub":
                    return updateConvert(session.getMapper(InflowMapper.class).updateSubMoney(inflow), DAO);
                default:
                    return new MyData(3007, null, "can not find operator");
            }

        } catch (Exception e) {
            return new MyData(3005, null, e.getMessage());
        }

    }

}
