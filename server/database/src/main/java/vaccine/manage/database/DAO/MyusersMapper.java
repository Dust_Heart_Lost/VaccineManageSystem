package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Myusers;

public interface MyusersMapper {
    List<Myusers> selectAll();

    int insert(Myusers record);

    int insertSelective(Myusers record);

    Myusers selectByUsername(String username);

    int updateByPrimaryKeySelective(Myusers record);

    int updateByPrimaryKey(Myusers record);

    List<Myusers> fuzzySearch(String searchModel);
}