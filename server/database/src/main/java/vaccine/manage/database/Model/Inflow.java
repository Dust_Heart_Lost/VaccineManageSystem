package vaccine.manage.database.Model;

import java.math.BigDecimal;

public class Inflow {
    private Integer id;

    private Integer vaccineid;

    private String date;

    private Integer myuserid;

    private Integer quantity;

    private BigDecimal price;

    private String name;

    private String job;

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVaccineid() {
        return vaccineid;
    }

    public void setVaccineid(Integer vaccineid) {
        this.vaccineid = vaccineid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getMyuserid() {
        return myuserid;
    }

    public void setMyuserid(Integer myuserid) {
        this.myuserid = myuserid;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}