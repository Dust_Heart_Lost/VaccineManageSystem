package vaccine.manage.database.Model;

import java.math.BigDecimal;

public class Record {
    private Integer id;

    private String date;

    private Integer vaccineID;

    private Integer vaccinatorID;

    private Integer processorID;

    private String vaccinatorNum;

    private String vaccinatorName;

    private String processorNum;

    private String processorName;

    private String num;

    private String name;

    private String unit;

    private String spec;

    private String kind;

    private String suppliersNum;

    private String suppliersName;

    private String address;

    private Integer quantity;

    private BigDecimal price;

    public Integer getVaccineID() {
        return vaccineID;
    }

    public void setVaccineID(Integer vaccineID) {
        this.vaccineID = vaccineID;
    }

    public Integer getVaccinatorID() {
        return vaccinatorID;
    }

    public void setVaccinatorID(Integer vaccinatorID) {
        this.vaccinatorID = vaccinatorID;
    }

    public Integer getProcessorID() {
        return processorID;
    }

    public void setProcessorID(Integer processorID) {
        this.processorID = processorID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVaccinatorNum() {
        return vaccinatorNum;
    }

    public void setVaccinatorNum(String vaccinatorNum) {
        this.vaccinatorNum = vaccinatorNum;
    }

    public String getVaccinatorName() {
        return vaccinatorName;
    }

    public void setVaccinatorName(String vaccinatorName) {
        this.vaccinatorName = vaccinatorName;
    }

    public String getProcessorNum() {
        return processorNum;
    }

    public void setProcessorNum(String processorNum) {
        this.processorNum = processorNum;
    }

    public String getProcessorName() {
        return processorName;
    }

    public void setProcessorName(String processorName) {
        this.processorName = processorName;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getSuppliersNum() {
        return suppliersNum;
    }

    public void setSuppliersNum(String suppliersNum) {
        this.suppliersNum = suppliersNum;
    }

    public String getSuppliersName() {
        return suppliersName;
    }

    public void setSuppliersName(String suppliersName) {
        this.suppliersName = suppliersName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

   
}