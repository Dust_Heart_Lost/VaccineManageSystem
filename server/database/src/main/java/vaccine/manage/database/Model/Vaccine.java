package vaccine.manage.database.Model;

import java.math.BigDecimal;

public class Vaccine {
    private Integer id;

    private String num;

    private String name;

    private Integer categoryid;

    private String unit;

    private String spec;

    private Integer suppliersID;

    private String note;

    private String kind;

    private String suppliersName;

    private String suppliersNum;

    private String address;

    private Integer  quantity;

    private BigDecimal money;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num == null ? null : num.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec == null ? null : spec.trim();
    }

    public Integer getSuppliersID() {
        return suppliersID;
    }

    public void setSuppliersID(Integer suppliersID) {
        this.suppliersID = suppliersID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind == null ? null : kind.trim();
    }

    public String getSuppliersName() {
        return suppliersName;
    }

    public void setSuppliersName(String suppliersName) {
        this.suppliersName = suppliersName == null ? null : suppliersName.trim();
    }

    public String getSuppliersNum() {
        return suppliersNum;
    }

    public void setSuppliersNum(String suppliersNum) {
        this.suppliersNum = suppliersNum == null ? null : suppliersNum.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }
}