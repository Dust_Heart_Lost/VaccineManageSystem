package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Suppliers;

public interface SuppliersMapper {
    List<Suppliers> selectAll();

    int deleteByPrimaryKey(Integer id);

    int insert(Suppliers record);

    int insertSelective(Suppliers record);

    Suppliers selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Suppliers record);

    int updateByPrimaryKey(Suppliers record);

    List<Suppliers> fuzzySearch(String searchModel);
}