package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Category;

public interface CategoryMapper {
    List<Category> selectAll();

    int insert(Category record);

    int insertSelective(Category record);

    Category selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);
    
    List<Category> fuzzySearch(String searchModel);
}