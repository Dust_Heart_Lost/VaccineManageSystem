package vaccine.manage.database.DAO;

import java.util.List;

import vaccine.manage.database.Model.Vaccine;

public interface VaccineMapper {
    List<Vaccine> selectAll();

    int insert(Vaccine record);

    int insertSelective(Vaccine record);

    Vaccine selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Vaccine record);

    int updateByPrimaryKey(Vaccine record);

    List<Vaccine> fuzzySearch(String searchModel);
}