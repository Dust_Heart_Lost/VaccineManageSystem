package vaccine.manage.database;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.Test;
import vaccine.manage.common.MyData;

public class DatabaseTest {
    private String url = "http://localhost:8763/";

    @Test
    public void queryAll() {
        JSONObject param = new JSONObject();
        param.put("startPage", 1);
        param.put("pageSize", 2);
        param.put("vaccineID", 2);
        MyData myData = new MyData(0, param, null);
        ArrayList<String> tables = new ArrayList<String>();
        // tables.add("Category");
        // tables.add("Detail");
        // tables.add("Inflow");
        // tables.add("Myusers");
        tables.add("Record");
        // tables.add("Roles");
        // tables.add("Stock");
        // tables.add("Storeroom");
        // tables.add("Suppliers");
        // tables.add("Vaccine");

        for (String table : tables) {
            String data = Post(url + "queryAll/" + table, myData);
            System.out.println(data);
            // assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);
        }

    }

    @Test
    public void queryById() {
        JSONObject param = new JSONObject();
        param.put("username", "gujing");
        MyData myData = new MyData(0, param, null);
        ArrayList<String> tables = new ArrayList<String>();
        // tables.add("Category");
        // tables.add("Detail");
        // tables.add("Inflow");
        tables.add("Myusers");
        // tables.add("Record");
        // tables.add("Roles");
        // tables.add("Stock");
        // tables.add("Storeroom");
        // tables.add("Suppliers");
        // tables.add("Vaccine");

        for (String table : tables) {
            String data = Post(url + "queryById/" + table, myData);
            System.out.println(data);
            assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);
        }

    }

    @Test
    public void insert() {
        JSONObject param = new JSONObject();
        param.put("vaccineID", "1");
        param.put("date", "2022-04-08 23:51:50");
        param.put("myUserID", "7");
        param.put("quantity", "1");
        param.put("price", "1.1");
        MyData myData = new MyData(0, param, null);
        ArrayList<String> tables = new ArrayList<String>();
        // tables.add("Category");
        // tables.add("Detail");
        tables.add("Inflow");
        // tables.add("Myusers");
        // tables.add("Record");
        // tables.add("Roles");
        // tables.add("Stock");
        // tables.add("Storeroom");
        // tables.add("Suppliers");
        // tables.add("Vaccine");

        for (String table : tables) {
            String data = Post(url + "insert/" + table, myData);
            System.out.println(data);
            assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);
        }

    }

    @Test
    public void update() {
        JSONObject param = new JSONObject();
        param.put("kind", "注射类");
        MyData myData = new MyData(0, param, null);
        ArrayList<String> tables = new ArrayList<String>();
        tables.add("Category");
        tables.add("Detail");
        tables.add("Inflow");
        tables.add("Myusers");
        tables.add("Record");
        tables.add("Roles");
        tables.add("Stock");
        tables.add("Storeroom");
        tables.add("Suppliers");
        tables.add("Vaccine");

        for (String table : tables) {
            String data = Post(url + "update/" + table, myData);
            System.out.println(data);
            assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);
        }

    }


    @Test
    public void search() {
        JSONObject param = new JSONObject();
        param.put("startPage", 1);
        param.put("pageSize", 2);
        param.put("searchModel","注射");
        MyData myData = new MyData(0, param, null);
        ArrayList<String> tables = new ArrayList<String>();
        tables.add("Category");
        tables.add("Detail");
        tables.add("Inflow");
        tables.add("Myusers");
        tables.add("Record");
        tables.add("Roles");
        tables.add("Stock");
        tables.add("Storeroom");
        tables.add("Suppliers");
        tables.add("Vaccine");

        for (String table : tables) {
            String data = Post(url + "search/" + table, myData);
            System.out.println(data);
            assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);
        }

    }

    @Test
    public void updateVaccineMoney() {
        JSONObject param = new JSONObject();
        param.put("vaccineID", "1");
        param.put("date", "2022-04-08 23:51:50");
        param.put("myUserID", "7");
        param.put("quantity", "1");
        param.put("price", "1.1");
        MyData myData = new MyData(0, param, null);
        ArrayList<String> tables = new ArrayList<String>();
        // tables.add("Category");
        // tables.add("Detail");
        // tables.add("Inflow");
        // tables.add("Myusers");
        // tables.add("Record");
        // tables.add("Roles");
        // tables.add("Stock");
        // tables.add("Storeroom");
        // tables.add("Suppliers");
        tables.add("add");

        for (String table : tables) {
            String data = Post(url + "updateVaccineMoney/" + table, myData);
            System.out.println(data);
            assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);
        }

    }

    public String Post(String urlString, MyData parameters) {
        OutputStreamWriter out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        HttpURLConnection conn = null;
        try {
            URL url = new URL(urlString);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            // 发送POST请求必须设置为true
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 设置连接超时时间和读取超时时间
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(10000);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            // 获取输出流
            out = new OutputStreamWriter(conn.getOutputStream());
            String jsonStr = JSONObject.toJSONString(parameters);
            out.write(jsonStr);
            out.flush();
            out.close();
            // 取得输入流，并使用Reader读取
            if (200 == conn.getResponseCode()) {
                in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line;
                while ((line = in.readLine()) != null) {
                    result.append(line);
                }
                return result.toString();
            } else {
                System.out.println("ResponseCode is an error code:" + conn.getResponseCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();

                }

            } catch (IOException ioe) {
                ioe.printStackTrace();
                return null;
            }

        }
        return null;
    }

}