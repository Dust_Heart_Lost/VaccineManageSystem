package vaccine.manage.gateway.controller;

import com.alibaba.fastjson.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import vaccine.manage.common.MyData;
import vaccine.manage.gateway.service.GatewayDatabaseService;

@RestController
public class GatewayController {

    @Autowired
    GatewayDatabaseService databaseService;

    /**
     * 错误码
     * 2001：无法找到页面
     * 2002：密码错误
     * 2003: 用户名错误
     * 
     */

    @PostMapping(value = "queryAll/{page}")
    public MyData queryALL(@RequestBody MyData parameters, @PathVariable String page) {
        switch (page) {
            case "vaccineinfo.kind":
                return databaseService.queryALL(parameters, "Category");
            case "vaccineinfo.suppliers":
                return databaseService.queryALL(parameters, "Suppliers");
            case "vaccineinfo.vaccine":
                return databaseService.queryALL(parameters, "Vaccine");
            case "vaccineinfo.storeroom":
                return databaseService.queryALL(parameters, "Storeroom");
            case "userinfo.roles":
                return databaseService.queryALL(parameters, "Roles");
            case "userinfo.myusers":
                return databaseService.queryALL(parameters, "Myusers");
            case "inventory.find":
                return databaseService.queryALL(parameters, "Vaccine");
            case "inventory.find.inflow":
                return databaseService.queryALL(parameters, "Inflow");
            case "vaccinate.record":
                return databaseService.queryALL(parameters, "Record");
            default:
                return new MyData(2001, null, "无法找到页面");
        }

    }

    @PostMapping(value = "insertNewData/{page}")
    public MyData insertNewData(@RequestBody MyData parameters, @PathVariable String page) {
        switch (page) {
            case "vaccineinfo.kind":
                return databaseService.insert(parameters, "Category");
            case "vaccineinfo.suppliers":
                return databaseService.insert(parameters, "Suppliers");
            case "vaccineinfo.vaccine":
                return databaseService.insert(parameters, "Vaccine");
            case "vaccineinfo.storeroom":
                return databaseService.insert(parameters, "Storeroom");
            case "userinfo.roles":
                return databaseService.insert(parameters, "Roles");
            case "userinfo.myusers":
                return databaseService.insert(parameters, "Myusers");
            case "inventory.in":
                MyData result1 = databaseService.insert(parameters, "Inflow");
                if (result1.getCode() == 1000) {
                    databaseService.updateVaccineMoney(parameters, "add");
                }
                return result1;
            case "vaccinate.out":
                MyData result = databaseService.insert(parameters, "Record");
                if (result.getCode() == 1000) {
                    databaseService.updateVaccineMoney(parameters, "sub");
                }
                return result;
            default:
                return new MyData(2001, null, "无法找到页面");
        }

    }

    @PostMapping(value = "updateData/{page}")
    public MyData updateData(@RequestBody MyData parameters, @PathVariable String page) {
        switch (page) {
            case "vaccineinfo.kind":
                return databaseService.updateData(parameters, "Category");
            case "vaccineinfo.suppliers":
                return databaseService.updateData(parameters, "Suppliers");
            case "vaccineinfo.vaccine":
                return databaseService.updateData(parameters, "Vaccine");
            case "vaccineinfo.storeroom":
                return databaseService.updateData(parameters, "Storeroom");
            case "userinfo.roles":
                return databaseService.updateData(parameters, "Roles");
            case "userinfo.myusers":
                return databaseService.updateData(parameters, "Myusers");
            case "vaccinate.out":
                return databaseService.updateData(parameters, "Record");
            default:
                return new MyData(2001, null, "无法找到页面");
        }

    }

    @PostMapping(value = "fuzzySearch/{page}")
    public MyData fuzzySearch(@RequestBody MyData parameters, @PathVariable String page) {
        switch (page) {
            case "vaccineinfo.kind":
                return databaseService.search(parameters, "Category");
            case "vaccineinfo.suppliers":
                return databaseService.search(parameters, "Suppliers");
            case "vaccineinfo.vaccine":
                return databaseService.search(parameters, "Vaccine");
            case "vaccineinfo.storeroom":
                return databaseService.search(parameters, "Storeroom");
            case "userinfo.roles":
                return databaseService.search(parameters, "Roles");
            case "userinfo.myusers":
                return databaseService.search(parameters, "Myusers");
            case "vaccinate.record":
                return databaseService.search(parameters, "Record");
            default:
                return new MyData(2001, null, "无法找到页面");
        }

    }

    @PostMapping(value = "login")
    public MyData login(@RequestBody MyData parameters) {
        JSONObject loginInfo = (JSONObject) JSONObject.toJSON(parameters.getData());
        JSONObject parameter = new JSONObject();
        parameter.put("username", loginInfo.get("username").toString());
        MyData userinfo = databaseService.queryById(new MyData(0, parameter, null), "Myusers");
        if (userinfo.getCode() == 1000) {
            return ((JSONObject) JSONObject.toJSON(userinfo.getData())).get("password").toString()
                    .equals(loginInfo.get("password").toString()) ? userinfo : new MyData(2002, null, "password error");
        } else {
            return new MyData(2003, null, "username error");
        }
    }
}
