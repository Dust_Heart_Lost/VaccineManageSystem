package vaccine.manage.gateway.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import vaccine.manage.common.MyData;

@Service
@FeignClient(value = "database")
public interface GatewayDatabaseService {
    @PostMapping(value = "queryAll/{table}")
    MyData queryALL(@RequestBody MyData parameters, @PathVariable String table);

    @PostMapping(value = "insert/{table}")
    MyData insert(@RequestBody MyData parameters, @PathVariable String table);

    @PostMapping(value = "update/{table}")
    MyData updateData(@RequestBody MyData parameters, @PathVariable String table);

    @PostMapping(value = "search/{table}")
    public MyData search(@RequestBody MyData parameters, @PathVariable String table);

    @PostMapping(value = "queryById/{table}")
    public MyData queryById(@RequestBody MyData parameters, @PathVariable String table);

    @PostMapping(value = "updateVaccineMoney/{table}")
    public MyData updateVaccineMoney(@RequestBody MyData parameters, @PathVariable String table);
}
