package vaccine.manage.gateway;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.alibaba.fastjson.JSONObject;

import org.junit.jupiter.api.Test;

import vaccine.manage.common.MyData;

class GatewayTests {
	private String url = "http://localhost:8761/";

	@Test
	public void queryAll() {
		JSONObject param = new JSONObject();
		param.put("startPage", 2);
		param.put("pageSize", 5);
		param.put("vaccineID", 2);
		MyData myData = new MyData(0, param, null);
		String data = Post(url+"queryAll/inventory.find.inflow", myData);
		System.out.println(data);
		assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);

	}

	@Test
	public void insertNewData() {
		JSONObject param = new JSONObject();
		param.put("kind", "注射类");
		MyData myData = new MyData(0, param, null);
		String data = Post(url+"insertNewData/vaccineinfo.kind", myData);
		System.out.println(data);
		assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);

	}

	@Test
	public void updateData() {
		JSONObject param = new JSONObject();
		param.put("kind", "注射");
		param.put("id", 24);
		MyData myData = new MyData(0, param, null);
		String data = Post(url+"updateData/vaccineinfo.kind", myData);
		System.out.println(data);
		assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);

	}

	@Test
	public void fuzzySearch() {
		JSONObject param = new JSONObject();
		param.put("startPage", 1);
		param.put("pageSize", 5);
		param.put("searchModel","222");
		MyData myData = new MyData(0, param, null);
		String data = Post(url+"fuzzySearch/vaccineinfo.kind", myData);
		System.out.println(data);
		assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);

	}

	@Test
	public void login() {
		JSONObject param = new JSONObject();
		param.put("username", "gujing");
		param.put("password", "123456");
		MyData myData = new MyData(0, param, null);
		String data = Post(url+"login", myData);
		System.out.println(data);
		assertEquals(Integer.valueOf(((JSONObject) JSONObject.parse(data)).get("code").toString()), 1000);

	}


	public String Post(String urlString, MyData parameters) {
		OutputStreamWriter out = null;
		BufferedReader in = null;
		StringBuilder result = new StringBuilder();
		HttpURLConnection conn = null;
		try {
			URL url = new URL(urlString);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			// 发送POST请求必须设置为true
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 设置连接超时时间和读取超时时间
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(10000);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept", "application/json");
			// 获取输出流
			out = new OutputStreamWriter(conn.getOutputStream());
			String jsonStr = JSONObject.toJSONString(parameters);
			out.write(jsonStr);
			out.flush();
			out.close();
			// 取得输入流，并使用Reader读取
			if (200 == conn.getResponseCode()) {
				in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
				String line;
				while ((line = in.readLine()) != null) {
					result.append(line);
					// System.out.println(line);
				}
				return result.toString();
			} else {
				System.out.println("ResponseCode is an error code:" + conn.getResponseCode());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();

				}

			} catch (IOException ioe) {
				ioe.printStackTrace();
				return null;
			}

		}
		return null;
	}

}
