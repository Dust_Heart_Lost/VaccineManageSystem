package vaccine.manage.common;

public class MyData {
   private Integer code;
   private Object data;
   private String msg;

   public MyData() {
   };

   public MyData(Integer code, Object data, String msg) {
      this.code = code;
      this.data = data;
      this.msg = msg;
   }

   public String getMsg() {
      return this.msg;
   }

   public Object getData() {
      return this.data;
   }

   public Integer getCode() {
      return this.code;
   }
}
