import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/layout/Layout.vue'

const routes = [
  {
    path: '/',
    name: 'Index',
    redirect: "/login",
    meta: {
      title: "首页"
    },
    // component: () => import('../components/LoginForm.vue')
    component: () => import('../views/layout/Layout.vue'),
    children: [
      {
        path: "/home",
        name: "Home",

        component: () => import('../views/layout/home/Home.vue'),
      }, {
        path: "/inventory",
        name: "Inventory",
        redirect: "/find",
        meta: {
          title: "库存管理"
        },
        component: () => import('../views/layout/inventory/Inventory.vue'),
        children: [
          {
            path: "/find",
            name: "Find",
            meta: {
              title: "库存查询"
            },
            component: () => import('../views/layout/inventory/find/Find.vue')
          },
          {
            path: "/in",
            name: "In",
            meta: {
              title: "入库"
            },
            component: () => import('../views/layout/inventory/in/In.vue')
          },
        ]
      }, {
        path: "/vaccineinfo",
        name: "VaccineInfo",
        redirect: "/vaccine",
        meta: {
          title: "疫苗基本信息管理"
        },
        component: () => import('../views/layout/vaccineinfo/VaccineInfo.vue'),
        children: [
          {
            path: "/vaccine",
            name: "Vaccine",
            meta: {
              title: "疫苗管理"
            },
            component: () => import('../views/layout/vaccineinfo/vaccine/Vaccine.vue')
          },
          {
            path: "/suppliers",
            name: "Suppliers",
            meta: {
              title: "供应商管理"
            },
            component: () => import('../views/layout/vaccineinfo/Suppliers.vue')
          },
          {
            path: "/kind",
            name: "Kind",
            meta: {
              title: "种类管理"
            },
            component: () => import('../views/layout//vaccineinfo/Kind.vue')
          },
          {
            path: "/storeroom",
            name: "storeroom",
            meta: {
              title: "库房管理"
            },
            component: () => import('../views/layout//vaccineinfo/Storeroom.vue')
          }
        ]
      }, {
        path: "/vaccinate",
        name: "Vaccinate",
        redirect: "/out",
        meta: {
          title: "接种管理"
        },
        component: () => import('../views/layout/vaccinate/Vaccinate.vue'),
        children: [
          {
            path: "/out",
            name: "Out",
            meta: {
              title: "接种"
            },
            component: () => import('../views/layout/vaccinate/out/Out.vue')
          },
          {
            path: "/record",
            name: "Record",
            meta: {
              title: "接种记录查询"
            },
            component: () => import('../views/layout/vaccinate/record/Record.vue')
          },
        ]
      }, {
        path: "/userinfo",
        name: "User",
        redirect: "/myusers",
        meta: {
          title: "用户基本信息管理"
        },
        component: () => import('../views/layout/user/User.vue'),
        children: [
          {
            path: "/myusers",
            name: "Myusers",
            meta: {
              title: "用户管理"
            },
            component: () => import('../views/layout/user/Myusers.vue')
          },
          {
            path: "/role",
            name: "Role",
            meta: {
              title: "角色管理"
            },
            component: () => import('../views/layout/user/Role.vue')
          },
        ]
      },{
        path: "/person",
        name: "Person",
        meta: {
          title: "个人中心"
        },
        component: () => import('../views/layout/person/Person.vue'),
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/loginandregister/LoginRegister.vue')
  },
  {
    path: '/:catchAll(.*)',
    name: '/404',
    component: () => import('../views/404.vue')
  }
]


const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


export default router


