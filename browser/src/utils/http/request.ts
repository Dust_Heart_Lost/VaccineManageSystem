import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import { useRouter } from "vue-router";

export class MyData {
    private code: number;
    private data: Object;
    private dataType: String;
    private msg: String;
    constructor(code: number, data: Object, dataType: String, msg: String) {
        this.code = code;
        this.data = data;
        this.dataType = dataType;
        this.msg = msg;
    }
    public setCode(code: number) {
        this.code = code;
    }
    public getCode(): number {
        return this.code;
    }
    public setMsg(msg: String) {
        this.msg = msg;
    }
    public getMsg(): String {
        return this.msg;
    }
    public setData(data: Object) {
        this.data = data;
    }
    public getData(): Object {
        return this.data;
    }
    public setDataType(dataType: String) {
        this.dataType = dataType;
    }
    public getDataType(data: String): String {
        return this.dataType
    }
};


export function request(config: any) {
    var instance = axios.create({
        timeout: 5000, //设置超时
        headers: {
            'Content-Type': 'application/json;charset=UTF-8;',
        }
    })
    //请求拦截器 
    instance.interceptors.request.use((config) => {
        //此处可做别的操作，例如权限校验
        return config;
    }, (error) => {
        return Promise.reject(error)
    });

    //响应拦截器
    instance.interceptors.response.use((response) => {
        // 此处可做别的操作，例如转码之类的
        return response.data;
    }, (error) => {
        const router = useRouter();
        if (error.response && error.response.status) {
            const status = error.response.status
            switch (status) {
                case 401:
                    router.push({ path: '/login' })
                    break;
                case 403:
                    router.push({ path: '/login' })
                    break;
                case 404:
                    router.push({ path: '/404' });
                    break;
                case 500:
                    break;
                default:
                    break;
            }
        }
        return Promise.reject(error);
    });


    return instance(config);
}