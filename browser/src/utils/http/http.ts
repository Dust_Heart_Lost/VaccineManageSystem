import { request } from "./request";

export const http = {
    get(url:String, params?:Object) {
        const config = {
            method: 'get',
            url: url,
            params:params
        }
        return request(config)
    },


    post(url:String, params?:Object) {
        const config = {
            method: 'post',
            url: url,
            data:params,
            crossDomain:true
        }
        return request(config)
    }
}