import { createStore } from 'vuex'

export default createStore({
  state: {
    userinfo:null
  },
  getters: {
    getUserInfo:(state)=>state.userinfo,
  },
  mutations: {
    setUserInfo:(state,userInfo)=>{
      state.userinfo=userInfo
    },
    loginOut:(state)=>{
      state.userinfo=null
    }
  },
  actions: {
  },
  modules: {
  }
})
