import { ref } from "vue";


// 校验规则
export const rules = ref({
    quantity: [
        {
            pattern: /^\+?[1-9]\d*$/,
            message: 'quantity must >0 ',
            trigger: 'blur',
            required: true
        },
    ],
    price: [
        {
            pattern: /^([1-9]{1}\d*(\.[0-9]){0,2})|(0\.[1-9][0-9])|(0\.0[1-9])$/,
            message: 'price must >0.00 ',
            trigger: 'blur',
            required: true
        },
    ],
});